import React from 'react';
import './App.css';
import Header from './components/header';
import Footer from './components/footer';
import Content from './components/content';

class App extends React.Component {
  
  constructor(props) {
    super(props);
    this.state ={
      title: 'Samvaad Shala',
      headerLinks: [
        {title: 'Home', path: '/'},
        {title: 'About', path: '/about'},
        {title: 'Contact Us', path: '/contact'},]
    
    }
  }
  
  render() {
    return (
      <>
       <Header />
       <Content />
       <Footer />
       </>

    );
     
    
  }
}

export default App;
