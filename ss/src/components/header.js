import React,{Component} from 'react';
import LOGO from"./LOGO.png"
import back from "./back.jpg"

const styles={
    logo: {
        width:"130px",
        height:"90px",
        border:"none",
        
    },
    container :
    {
        backgroundColor: "white",
        color:"black",
        display:"flex",
        padding:"1px 16px 1px 16px",
        height:"90px"
       
    },
    headerlinks :{
        width:"100%",
        display:"flex",
        flexDirection:"row",
        justifyContent:"space-around"
        
        
    },
    background:{
        backgroundImage: "url(" +  back + ")",
        backgroundSize: 'cover',
        height:"70vh",
        display:'flex',
       


    },
    home:{
        marginLeft:"auto",
        padding:10,
        color:"#0a2d3d",
        fontSize:"20px"
    },
    training:{
        marginLeft:20,
        padding:10,
        color:"#0a2d3d",
        fontSize:"20px"

    },
    blogs: {
        marginLeft: 20,
        padding:10,
        color:"#0a2d3d",
        fontSize:"20px"
    },
     overview:{
        marginLeft: 20,
        marginRight: 15,
        padding:10,
        color:"#0a2d3d",
        fontSize:"20px"
    },
     login:{
        marginLeft: 20,
        marginRight: 15,
        padding:10,
        color:"#0a2d3d",
        fontSize:"20px"
    },
    vision:{
        color:"white",
        display:"flex",
        width:"350px",
        padding:"120px 0px 0px 35px"        
    },
    form:{
        display:"flex",
        flexDirection:"column",
        justifyContent:"center",
        marginLeft:"auto",
        border: "none ",
        borderRadius: "3px",
        height:"65vh",
        marginRight:60,
        marginTop:"10px",   
        width:"300px"    
       
        
    },
    button1:{
        display: "inline-block",
        padding: "0.35em 1.2em",
        border: '0.1em solid black',
        margin: "5px 4em 0.3em 4em",
        borderRadius: "0.12em",
        boxSizing: "border-box",
        textDecoration: "none",
        fontSize:"16px",
        fontWeight: "300px",
        color: "black",
        textAlign: "center",
        transition: "all 0.2s"
    
    },
    email:{
        width:"270px",
        height:"24px",
        border:"1px solid black",
        borderRadius:"3px"
    }
   



}



class Header extends Component {
render(){
    
    
    return(

<header>
    <div className="outer">

    <div className="container" style={styles.container}>

    <div className="logo" >
        <img src={LOGO} alt={"SamvaadShalaLogo"} style={styles.logo}></img>
      </div>
      <div className="headerlinks" style={styles.headerlinks}> 
     <p style={styles.home}>Home</p>
     <p style={styles.training}>Trainings</p>
     <p style={styles.blogs}>Blogs</p>
     <p style={styles.overview}>Overview</p>
     <p style={styles.login}>Login</p>
      </div>
    </div>
      <div className="background"   style={styles.background}>
          <div className="vision"  style={styles.vision}>
              <h1>"There goes the random text about the website"</h1>
          </div>

          <div className="form" style={{marginLeft:"auto"}}>

          <form action=""   style={styles.form}>
          {/* <h1 style={{marginTop:"0px", marginLeft:"88px"}}>Register</h1> */}

    <label for="email" className="email" ><b>Email</b></label><br></br>
    <input type="text" placeholder="Enter Email" name="email"  style={styles.email} required></input><br></br>

    <label for="psw" className="email" ><b>Password</b></label><br></br>
    <input type="password" placeholder="Enter Password" name="psw"  className="email" style={styles.email} required></input><br></br>
    <label for="psw" className="email" ><b>Repeat Password</b></label><br></br>
    <input type="password" placeholder="Confirm Password" name="cnfpsw"  className="email" style={styles.email} required></input><br></br>
    <a href="#" className="button1" style={styles.button1}>Register</a>

    
</form>
          </div>
</div>
            
    </div>
        
</header>
    );

}
}
export default Header;